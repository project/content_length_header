<?php

namespace Drupal\content_length_header;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * ContentLengthHeaderMiddleware which appends Content-Length header.
 */
class ContentLengthHeaderMiddleware implements HttpKernelInterface {

  /**
   * The kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * Constructs the ContentLengthHeaderMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    $response = $this->httpKernel->handle($request, $type, $catch);

    // Inspired by \Symfony\Component\HttpKernel\HttpCache\HttpCache.
    if (!$response->headers->has('Transfer-Encoding')) {
      $response->headers->set('Content-Length', \strlen($response->getContent()));
    }

    return $response;
  }

}
