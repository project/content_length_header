<?php

namespace Drupal\Tests\content_length_header\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the content length header.
 *
 * @group content_length_header
 */
class ContentLengthHeaderTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'content_length_header',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that the content length header is added.
   */
  public function testContentLengthHeaderExists() {
    $this->drupalGet('<front>');
    $this->assertSession()->responseHeaderExists('Content-Length');
  }

}
